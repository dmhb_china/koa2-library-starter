const jwt = require('jsonwebtoken')

const auth = async (ctx, next) => {
  const token = ctx.get('token')
  if (!token) {
    throw global.Ex.Params({
      errMsg: 'token不能为空'
    })
  }

  const { secretKey } = global.config.security
  jwt.verify(token, secretKey, (err, res) => {
    if (err) {
      throw new global.Ex.AuthFail({
        errMsg: '无效的token'
      })
    }
    // 将用户信息挂载到ctx
    ctx.user = res.userInfo
  })

  await next()
}

module.exports = auth